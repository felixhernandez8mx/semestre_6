<?php
 
require_once 'vendor/autoload.php';
 
$app = new Silex\Application();
 ini_set('display_errors', 1);
error_reporting(-1);
$app['debug'] = true;
$app->get('/clima/{city}/{country}', function($city,$country) use ($app) {

$client = new GuzzleHttp\Client();

$res = $client->request('GET', "http://api.openweathermap.org/data/2.5/forecast?id={$city}&APPID=fe9f4bea0a4d87b285632e9023f8ee0f&q={$country}");

$decode = json_decode($res->getBody());
// {"type":"User"...'
 
return $app->json($decode);
});
 
$app->run();




